using System.Collections.Generic;
using System.Security.Principal;

namespace Hashcode2020Qualification.Models
{
    public class OutputModel
    {
        public int Libraries { get; set; }
        public List<LibrarySubmission> LibrarySubmissions { get; set; }

        public OutputModel()
        {
            Libraries = 0;
            LibrarySubmissions = new List<LibrarySubmission>();
        }
    }

    public class LibrarySubmission
    {
        public int LibraryId { get; set; }
        public List<int> BooksToSend { get; set; }
    }
}