﻿using System.Collections.Generic;
using System.Linq;

namespace Hashcode2020Qualification.Models
{
    public struct InputModel
    {
        public Context Context { get; set; }
    }

    public class Context
    {
        public int TotalBooks { get; set; }
        public Book[] AllBooks { get; set; }
        public int TotalLibraries { get; set; }
        public int TotalDays { get; set; }
        public int CurrentDay { get; set; }
        public List<Library> Libraries { get; set; }

        public Context()
        {
            Libraries = new List<Library>();
            CurrentDay = 0;
        }
    }

    public class Library
    {
        public int Id { get; set; }
        public int InitialBooks { get; set; }
        public int SignupDays { get; set; }
        public int ScansPerDay { get; set; }
        public List<Book> Books { get; set; }
        public bool IsSignedUp { get; set; }

        public double Potential { get; set; }


        public Library()
        {
            Books = new List<Book>();
        }
        

        public double CalcPotential(int daysLeft)
        {
            if(daysLeft - SignupDays <= 0)
            {
                return double.MinValue;
            }

            if (Books.Any(b => b.Submitted))
            {
                Books = Books.Where(book => !book.Submitted).ToList();
            }

            if(Books.Count == 0)
            {
                return double.MinValue;
            }
            var max = (daysLeft - SignupDays) * (long)ScansPerDay;
            if (max > int.MaxValue)
            {
                max = Books.Count / 2;
            }
            var takenBooks = Books.Take((int)max).Select(b => b.Value).Sum() / SignupDays;
            Potential = takenBooks;
            return Potential;
        }
    }

    public class Book
    {
        public int Id { get; set; }
        public int Value { get; set; }
        public bool Submitted { get; set; }
    }
}