param(
    [bool]$zip = $true
)

Write-Output "Cleaning"
Remove-Item -Recurse -Force output

if ($zip)
{
    Write-Output "Zipping source code"

    $cwd = Get-Location
    Set-Location ..
    mkdir HC_TMP
    Set-Location HC_TMP
    $tmpDir = Get-Location
    Copy-Item $cwd\* $tmpDir -Recurse

    Remove-Item -Recurse -Force .idea
    Remove-Item -Recurse -Force input

    Set-Location $cwd
    mkdir output
    Compress-Archive $tmpDir ./output/code.zip -CompressionLevel Optimal

    Set-Location $cwd
    Remove-Item $tmpDir -Recurse -Force
}
else
{
    Write-Output "Skipping zipping source code"
}

Write-Output "Building"
dotnet publish -c Release -o output/build Console/Console.csproj -v q


Write-Output "Executing"
$files = Get-ChildItem input

#Start-Process powershell -WorkingDirectory $cwd -ArgumentList ("-NoExit", {
    output/build/Console.exe ./input/a_example.txt ./output
#})
#Start-Process powershell -WorkingDirectory $cwd -ArgumentList ("-NoExit", {
    output/build/Console.exe ./input/b_read_on.txt ./output
#})
#Start-Process powershell -WorkingDirectory $cwd -ArgumentList ("-NoExit", {
    output/build/Console.exe ./input/c_incunabula.txt ./output
#})
#Start-Process powershell -WorkingDirectory $cwd -ArgumentList ("-NoExit", {
    output/build/Console.exe ./input/d_tough_choices.txt ./output
#})
#Start-Process powershell -WorkingDirectory $cwd -ArgumentList ("-NoExit", {
    output/build/Console.exe ./input/e_so_many_books.txt ./output
#})
#Start-Process powershell -WorkingDirectory $cwd -ArgumentList ("-NoExit", {
    output/build/Console.exe ./input/f_libraries_of_the_world.txt ./output
#})