﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hashcode2020Qualification.Models;

namespace Hashcode2020Qualification.Algo
{
    public static class Algorithm
    {
        public static Task<int> CalculateScore(OutputModel output, InputModel input)
        {
            var score = output.LibrarySubmissions.SelectMany(c => c.BooksToSend).Distinct()
                .Select(i => input.Context.AllBooks[i].Value).Sum();
            return Task.FromResult(score);
        }

        public static Task<OutputModel> ProcessInput(InputModel input)
        {
            var inputContext = input.Context;
            var outputFile = new OutputModel();

            inputContext.Libraries.ForEach(l => l.CalcPotential(input.Context.TotalDays));
            // var sortedLibs = inputContext.Libraries.Sort((library, library1) => library.Score.CompareTo(library1.Score));
            var sortedLibs = inputContext.Libraries.OrderByDescending(l => l.Potential).ToList();
            var recalc = 1;
            if(input.Context.TotalDays > 10000)
            {
                recalc = 2;
            }

            while (sortedLibs.Count > 0 && inputContext.CurrentDay <= inputContext.TotalDays)
            {
                if (inputContext.CurrentDay % 100 == 0)
                {
                 //   Console.WriteLine("Processing day " + inputContext.CurrentDay);
                }

                var lib = sortedLibs[0];
                var daysAfterSignup = inputContext.TotalDays - (inputContext.CurrentDay + lib.SignupDays);
                var maxSubmissions = (long)lib.ScansPerDay * daysAfterSignup;
                if(maxSubmissions > int.MaxValue)
                {
                    maxSubmissions = int.MaxValue;
                }
                var booksToSubmit = lib.Books.Take((int)maxSubmissions).ToList();
                booksToSubmit.ForEach(b => b.Submitted = true);

                if (booksToSubmit.Count > 0)
                {
                    outputFile.LibrarySubmissions.Add(new LibrarySubmission()
                    {
                        LibraryId = lib.Id,
                        BooksToSend = booksToSubmit.Select(b => b.Id).ToList()
                    });
                    outputFile.Libraries++;
                    inputContext.CurrentDay += lib.SignupDays;
                }
                else
                {
                    break;
                }

                sortedLibs.RemoveAt(0);

                if (inputContext.CurrentDay % 10 == recalc)
                {
                    sortedLibs.AsParallel().ForAll(l => l.CalcPotential(daysAfterSignup));
                    sortedLibs = sortedLibs.Where(l => l.Potential != double.MinValue).OrderByDescending(l => l.Potential).ToList();
                }
            }

            return Task.FromResult(outputFile);
        }
    }
}