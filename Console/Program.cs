﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Hashcode2020Qualification.Models;
using Hashcode2020Qualification.Algo;

namespace Hashcode2020Qualification
{
    class Program
    {
        static void Main(string[] args)
        {
            var inputFile = args[0];
            var outputFolder = args[1];

            var model = Run(inputFile);
            var prepTask = PrepareOutputFolder(outputFolder, inputFile);

            Task.WaitAll(model, prepTask);

            WriteOutput(model.Result, prepTask.Result);
        }

        private static async Task<OutputModel> Run(string inputFile)
        {
            var watch = new Stopwatch();
            watch.Start();
            var input = await ReadInput(inputFile);
            //Console.WriteLine($"[{inputFile.PadLeft(25)}] Read time: {watch.ElapsedMilliseconds} ms");
            watch.Restart();
            var processed = await Algorithm.ProcessInput(input);
            watch.Stop();
            //Console.WriteLine($"[{inputFile.PadLeft(25)}] Processing time: {watch.ElapsedMilliseconds} ms");
            var score = await Algorithm.CalculateScore(processed, input);
            Console.WriteLine("Score: " + score);
            return processed;
        }

        private static void WriteOutput(OutputModel model, string file)
        {
            var lines = new List<string>(0);
            lines.Add(model.Libraries.ToString());
            foreach (var librarySubmission in model.LibrarySubmissions)
            {
                lines.Add($"{librarySubmission.LibraryId} {librarySubmission.BooksToSend.Count}");
                lines.Add(string.Join(" ", librarySubmission.BooksToSend));
            }
            File.WriteAllLines(file, lines);
        }

        private static Task<string> PrepareOutputFolder(string outputFolder, string inputFile)
        {
            return Task.Run(() =>
            {
                if (!File.Exists(inputFile)) throw new ArgumentException("Input file does not exist");
                Directory.CreateDirectory(outputFolder);

                var filenameParts = Path.GetFileName(inputFile)?.Split('.');
                if (filenameParts == null || filenameParts.Length <= 0)
                    throw new Exception("The filename could not be split into parts");
                var path =
                    $"{Path.TrimEndingDirectorySeparator(outputFolder)}{Path.DirectorySeparatorChar}{filenameParts[0]}.out";

                return path;
            });
        }

        private static async Task<InputModel> ReadInput(string inputFile)
        {
            if (!File.Exists(inputFile)) throw new ArgumentException("Input file does not exist");
            var lines = await File.ReadAllLinesAsync(inputFile);

            var lastLine = lines.Length - 1;
            while (string.IsNullOrEmpty(lines[lastLine]))
            {
                lastLine--;
            }
            var context = new Context();

            var firstLines = GetValues(lines[0]);
            context.TotalBooks = firstLines[0];
            context.TotalLibraries = firstLines[1];
            context.TotalDays = firstLines[2];

            var books = GetValues(lines[1]).Select((c, i) => new Book { Id = i, Value = c }).OrderBy(i => i.Id).ToArray();
            context.AllBooks = books;

            var numOftasks = (lastLine / 2) - 1;
            for (var index = 0; index <= numOftasks; index++)
            {
                var i = (index + 1) * 2;
                var libraryInfo = GetValues(lines[i]);
                var booksInLib = GetValues(lines[i + 1]);

                var library = new Library { Id = index, InitialBooks = libraryInfo[0], SignupDays = libraryInfo[1], ScansPerDay = libraryInfo[2] };
                context.Libraries.Add(library);
                library.Books.AddRange(booksInLib.Select(c => books[c]).OrderByDescending(c => c.Value));
            }

            return new InputModel
            {
                Context = context
            };
        }

        private static int[] GetValues(string line)
        {
            return line.Split(" ").Select(c => int.Parse(c)).ToArray();
        }
    }
}